// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif
#include <iostream>
#include <dune/common/parallel/mpihelper.hh> // An initializer of MPI
#include <dune/common/exceptions.hh> // We use exceptions
#include <dune/common/math.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/istl/matrix.hh>
#include <dune/pdelab/gridfunctionspace/gridfunctionspace.hh>
#include <dune/pdelab/gridoperator/gridoperator.hh>
#include <dune/pdelab/function/callableadapter.hh>
#include <dune/pdelab/constraints/conforming.hh>
#include <dune/pdelab/constraints/common/constraints.hh>
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/stationary/linearproblem.hh>

#include <dune/blockstructured-preconditioner/finiteelementmap/blockstructuredqkfem.hh>
#include <dune/blockstructured-preconditioner/localoperator/poisson.hh>
#include <dune/blockstructured-preconditioner/localoperator/local_to_global.hh>
#include <dune/blockstructured-preconditioner/localoperator/domain_count.hh>

#include <dune/grid/io/file/vtk.hh>
#include <dune/pdelab/gridfunctionspace/vtk.hh>
#include <dune/istl/io.hh>


constexpr std::size_t NBLOCKS = 3;
constexpr int dim = 2;

using TrueGrid = Dune::YaspGrid<dim>;
using TrueGV = typename TrueGrid::LeafGridView;
using DF = typename TrueGrid::ctype;
using RT = double;


struct Params{
  template<typename X>
  static bool is_dirichlet(const X& x){
    return true;
  }

  static RT f(const Dune::FieldVector<RT, dim>& x){
    const RT freq = Dune::StandardMathematicalConstants<RT>::pi();
    return 2 * freq * freq * (std::sin(freq * x[0]) * std::sin(freq * x[1]));
//    return 2 * x[1] * (1 - x[1]) + 2 * x[0] * (1 - x[0]);
  }

  static RT g(const Dune::FieldVector<RT, dim>& x){
    return 0;
  }

  static RT exact(const Dune::FieldVector<RT, dim>& x){
    const RT freq = Dune::StandardMathematicalConstants<RT>::pi();
    return std::sin(freq * x[0]) * std::sin(freq * x[1]);
//    return x[0] * (1 - x[0]) * x[1] * (1 - x[1]);
  }
};


template<typename FEM, typename LOP>
auto generate_matrix(const TrueGV& gv, const FEM& fem, LOP& lop){
  using GFS = Dune::PDELab::GridFunctionSpace<TrueGV, FEM, Dune::PDELab::NoConstraints>;
  GFS gfs(gv, fem);

  using MB = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
  using GOP = Dune::PDELab::GridOperator<GFS, GFS, LOP, MB, RT, RT, RT>;
  GOP go(gfs, gfs, lop, MB(9));

  using X = typename GOP::Domain;
  X x(gfs, 0.0);

  using J = typename GOP::Jacobian;
  J M(go);
  go.jacobian(x, M);

  return M;
}

template<typename FEMU, typename FEMV, typename LOP>
auto generate_matrix(const TrueGV& gv, const FEMU& femu, const FEMV& femv, LOP& lop){
  using GFSU = Dune::PDELab::GridFunctionSpace<TrueGV, FEMU, Dune::PDELab::NoConstraints>;
  GFSU gfsu(gv, femu);

  using GFSV = Dune::PDELab::GridFunctionSpace<TrueGV, FEMV, Dune::PDELab::NoConstraints>;
  GFSV gfsv(gv, femv);

  using MB = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
  using GOP = Dune::PDELab::GridOperator<GFSU, GFSV, LOP, MB, RT, RT, RT>;
  GOP go(gfsu, gfsv, lop, MB(9));

  using X = typename GOP::Domain;
  X x(gfsu, 0.0);

  using J = typename GOP::Jacobian;
  J M(go);
  go.jacobian(x, M);

  return M;
}

template <typename FEM, typename P>
auto generate_dirichlet_nodes(const TrueGV& gv, const FEM& fem, const P& p){
  using CON = Dune::PDELab::ConformingDirichletConstraints;

  using GFS = Dune::PDELab::GridFunctionSpace<TrueGV, FEM, CON>;
  GFS gfs(gv, fem);

  using CC = typename GFS::template ConstraintsContainer<RT>::Type;
  CC cc = {};
  cc.clear();

  auto bctype = Dune::PDELab::makeBoundaryConditionFromCallable(gv, [&p](const auto& x) {return p.is_dirichlet(x);});
  Dune::PDELab::constraints(bctype, gfs, cc);

  using X = Dune::PDELab::Backend::Vector<GFS, RT>;
  X x(gfs, 0.0);
  Dune::PDELab::set_constrained_dofs(cc, 1., x);

  return x;
}

template<typename FEM, typename LOP, typename P>
auto generate_rhs(const TrueGV& gv, const FEM& fem, LOP& lop, const P& p){
  using CON = Dune::PDELab::ConformingDirichletConstraints;
  using GFS = Dune::PDELab::GridFunctionSpace<TrueGV, FEM, Dune::PDELab::NoConstraints>;
  GFS gfs(gv, fem);

//  using CC = typename GFS::template ConstraintsContainer<RT>::Type;
//  CC cc = {};
//  cc.clear();
//
//  auto bctype = Dune::PDELab::makeBoundaryConditionFromCallable(gv, [&p](const auto& x) {return p.is_dirichlet(x);});
//  Dune::PDELab::constraints(bctype, gfs, cc);

  using MB = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
  using GOP = Dune::PDELab::GridOperator<GFS, GFS, LOP, MB, RT, RT, RT>;
  GOP go(gfs, gfs, lop, MB(9));

  using X = typename GOP::Domain;
  X x(gfs, 0.0);

//  auto g = Dune::PDELab::makeGridFunctionFromCallable(gv, [&p](const auto& x){return p.exact(x);});
//  Dune::PDELab::interpolate(g, gfs, x);
//  Dune::PDELab::set_nonconstrained_dofs(cc, 0., x);

  using Y = typename GOP::Range;
  Y r(gfs, 0.0);
  go.residual(x, r);

  return r;
}

template<typename FEM, typename LOP, typename P>
auto generate_exact(const TrueGV& gv, const FEM& fem, LOP& lop, const P& p){
  using CON = Dune::PDELab::ConformingDirichletConstraints;
  using GFS = Dune::PDELab::GridFunctionSpace<TrueGV, FEM, Dune::PDELab::NoConstraints>;
  GFS gfs(gv, fem);

//  using CC = typename GFS::template ConstraintsContainer<RT>::Type;
//  CC cc = {};
//  cc.clear();
//
//  auto bctype = Dune::PDELab::makeBoundaryConditionFromCallable(gv, [&p](const auto& x) {return p.is_dirichlet(x);});
//  Dune::PDELab::constraints(bctype, gfs, cc);

  using MB = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
  using GOP = Dune::PDELab::GridOperator<GFS, GFS, LOP, MB, RT, RT, RT>;
  GOP go(gfs, gfs, lop, MB(9));

  using X = typename GOP::Domain;
  X x(gfs, 0.0);

  auto g = Dune::PDELab::makeGridFunctionFromCallable(gv, [&p](const auto& x){return p.exact(x);});
  Dune::PDELab::interpolate(g, gfs, x);

  return x;
}

void constrain_dofs(Dune::BCRSMatrix<Dune::FieldMatrix<double, 1, 1>>& M,
                    const Dune::BlockVector<Dune::FieldVector<double, 1>>& dirichlet_dofs){
  for(auto row = M.begin(); row != M.end(); ++row){
    if(dirichlet_dofs[row.index()] > 0.){
      for(auto col = row->begin(); col != row->end(); ++ col){
        M[row.index()][col.index()] = 0.;
        M[col.index()][row.index()] = 0.;
      }
      M[row.index()][row.index()] = 1.;
    }
  }
}

template<typename FEM>
auto generate_domain_count(const TrueGV& gv, const FEM& fem){
  using LOP = DomainCount<FEM::blocks>;
  LOP lop{};

  using GFS = Dune::PDELab::GridFunctionSpace<TrueGV, FEM, Dune::PDELab::NoConstraints>;
  GFS gfs(gv, fem);

  using MB = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
  using GOP = Dune::PDELab::GridOperator<GFS, GFS, LOP, MB, RT, RT, RT>;
  GOP go(gfs, gfs, lop, MB(9));

  using X = typename GOP::Domain;
  X x(gfs, 0.0), dc(gfs, 0.0);

  go.residual(x, dc);

  return dc.storage();
}

Dune::Matrix<double> to_dense(const Dune::BCRSMatrix<Dune::FieldMatrix<double, 1, 1>>& M){
  Dune::Matrix<double> A(M.N(), M.M());

  A = 0.;
  for(auto row = M.begin(); row != M.end(); ++row)
    for(auto col = row->begin(); col != row->end(); ++col)
      A[row.index()][col.index()] = *col;

  return A;
}

void solve(const TrueGV& gv){
  using FEM = Dune::PDELab::QkBlockstructuredLocalFiniteElementMap<RT, RT, NBLOCKS, dim>;
  FEM fem;

  using CON = Dune::PDELab::ConformingDirichletConstraints;

  using GFS = Dune::PDELab::GridFunctionSpace<TrueGV, FEM, CON>;
  GFS gfs(gv, fem);
  gfs.name("q1");
  gfs.update();

  Params params;

  using CC = typename GFS::template ConstraintsContainer<RT>::Type;
  CC cc = {};
  cc.clear();

  auto bctype_lambda = [&](const auto& x){ return params.is_dirichlet(x); };
  auto bctype = Dune::PDELab::makeBoundaryConditionFromCallable(gv, bctype_lambda);
  Dune::PDELab::constraints(bctype, gfs, cc);

  using LOP = PoissonLocalOperator<Params, NBLOCKS>;
  LOP lop{params};

  using MB = Dune::PDELab::ISTL::BCRSMatrixBackend<>;
  using GOP = Dune::PDELab::GridOperator<GFS, GFS, LOP, MB, RT, RT, RT, CC, CC>;
  GOP go(gfs, cc, gfs, cc, lop, MB(9));

  using X = Dune::PDELab::Backend::Vector<GFS,DF>;
  X x_r(gfs), z(gfs);
  x_r = 0.0;
  auto g = Dune::PDELab::makeGridFunctionFromCallable(gv, [&](const auto& x){ return params.g(x); });
  Dune::PDELab::interpolate(g, gfs, x_r);

  using LS = Dune::PDELab::ISTLBackend_SEQ_SuperLU;
  LS ls{};
  Dune::PDELab::StationaryLinearProblemSolver<GOP, LS, X> slp(go, ls, x_r, 1e-10);

  slp.apply();

  using VTKWriter = Dune::SubsamplingVTKWriter<TrueGV>;
  Dune::RefinementIntervals subint(NBLOCKS);
  VTKWriter vtkwriter(gv, subint);
  std::string vtkfile = "poisson_";
  Dune::PDELab::addSolutionToVTKWriter(vtkwriter, gfs, x_r, Dune::PDELab::vtk::defaultNameScheme());
  vtkwriter.write(vtkfile, Dune::VTK::ascii);
}


int main(int argc, char** argv) {
  using Dune::PDELab::Backend::Native;
  using Dune::PDELab::Backend::native;
  // Maybe initialize MPI
  Dune::FakeMPIHelper::instance(argc, argv);

  int n_elements = 3;
  if (argc > 1)
    n_elements = std::stoi(argv[1]);

  TrueGrid grid({1., 1.}, {n_elements, n_elements});
  TrueGV gv = grid.leafGridView();

//  solve(gv);

  Params params;

  using LOP = PoissonLocalOperator<Params, NBLOCKS>;
  LOP lop{params};

  using FEM = Dune::PDELab::QkBlockstructuredLocalFiniteElementMap<RT, RT, NBLOCKS, dim>;
  FEM fem = {};

  using FEM_DG = Dune::PDELab::QkBlockstructuredDGLocalFiniteElementMap<RT, RT, NBLOCKS, dim>;
  FEM_DG fem_dg = {};

  auto M = generate_matrix(gv, fem, lop);
  auto rhs = generate_rhs(gv, fem, lop, params);

//  auto M_DG = generate_matrix(gv, fem_dg, lop);
//
//  using LTG = LocalToGlobalLocalOperator;
//  LTG ltg;
//  auto B = generate_matrix(gv, fem_dg, fem, ltg);
//
//  auto d_M = to_dense(*M);
//  auto d_M_DG = to_dense(*M_DG);
//  auto d_B = to_dense(*B);
//
//  auto d_M2 = d_B * d_M_DG * d_B.transpose();
//  d_M2 -= d_M;
//  std::cout << d_M2.frobenius_norm() << std::endl;
//
  auto dir = generate_dirichlet_nodes(gv, fem, params);
//
//
//  auto rhs_dg = generate_rhs(gv, fem_dg, lop, params);
//
//  auto dc = generate_domain_count(gv, fem);
//
//  auto dc_dg = *rhs_dg;
//
//  d_B.mtv(*dc, dc_dg);
//
//  auto dir_dg = *rhs_dg;
//  d_B.mtv(*dir, dir_dg);
//  for (int i = 0; i < rhs_dg->N(); ++i) {
//    if(dir_dg[i] > 0){
//      (*rhs_dg)[i] = 0;
//    }
//  }
//
//  auto rhs_diff = *rhs;
//  d_B.mmv(*rhs_dg, rhs_diff);
//
//  std::cout << rhs_diff.two_norm()/rhs->two_norm() << std::endl;
//
//  constrain_dofs(*M, *dir);

  for (int i = 0; i < dir.N(); ++i) {
    if(native(dir)[i]){
      native(M)[i] *= 0.;
      native(M)[i][i] = 1.;
      native(rhs)[i] = 0.;
    }
  }
//  Dune::printvector(std::cout, native(rhs), "", "");

  using CON = Dune::PDELab::ConformingDirichletConstraints;

  using GFS = Dune::PDELab::GridFunctionSpace<TrueGV, FEM, CON>;
  GFS gfs(gv, fem);
  gfs.name("q1");
  gfs.update();

  using X = Dune::PDELab::Backend::Vector<GFS,DF>;
  X x_r(gfs), z(gfs), rhs2(gfs);
  x_r = 0.0;
  native(rhs2) = native(rhs);

  using LS = Dune::PDELab::ISTLBackend_SEQ_SuperLU;
  LS ls{2};
  ls.apply(M, z, rhs, 1e-10);

  x_r -= z;

  auto exact = generate_exact(gv, fem, lop, params);

  X f(gfs);
  native(M).mv(native(exact), native(f));

//  for (int i = 0; i < rhs2.N(); ++i) {
//    std::cout << i << "\t" << native(rhs2)[i] << "\t" << native(f)[i] << std::endl;
//  }
  using VTKWriter = Dune::SubsamplingVTKWriter<TrueGV>;
  Dune::RefinementIntervals subint(NBLOCKS);
  VTKWriter vtkwriter(gv, subint);
  std::string vtkfile = "poisson";
  Dune::PDELab::addSolutionToVTKWriter(vtkwriter, gfs, x_r, Dune::PDELab::vtk::defaultNameScheme());
  vtkwriter.write(vtkfile, Dune::VTK::ascii);


  using VTKWriter = Dune::SubsamplingVTKWriter<TrueGV>;
  VTKWriter vtkwriter_e(gv, subint);
  std::string vtkfile_e = "exact";
  Dune::PDELab::addSolutionToVTKWriter(vtkwriter_e, gfs, exact, Dune::PDELab::vtk::defaultNameScheme());
  vtkwriter_e.write(vtkfile_e, Dune::VTK::ascii);


  native(rhs2) += native(f);
  native(x_r) -= native(exact);
//  Dune::printvector(std::cout, native(rhs2), "", "");
  std::cout << rhs2.two_norm()/f.two_norm() << std::endl;
  std::cout << x_r.two_norm()/exact.two_norm() << std::endl;

}
