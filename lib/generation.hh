#ifndef DUNE_BLOCKSTRUCTURED_PRECONDITIONER_GENERATION_HH
#define DUNE_BLOCKSTRUCTURED_PRECONDITIONER_GENERATION_HH

#include <dune/blockstructured-preconditioner/finiteelementmap/blockstructuredqkfem.hh>
#include "grid.hh"
#include "params.hh"

using FEM_CG = Dune::PDELab::QkBlockstructuredLocalFiniteElementMap<DF, RT, NBLOCKS, dim>;
using FEM_DG = Dune::PDELab::QkBlockstructuredDGLocalFiniteElementMap<DF, RT, NBLOCKS, dim>;
using FEM_COARSE = Dune::PDELab::QkBlockstructuredLocalFiniteElementMap<DF, RT, 1, dim>;

using VB = Dune::PDELab::NumPyVectorBackend;
using MB = Dune::PDELab::NumPyMatrixBackend;
using V = Dune::PDELab::numpy_vector<RT>;
using M = Dune::PDELab::Simple::SparseMatrixData<Dune::PDELab::numpy_vector, RT, std::size_t>;

template <typename FEM>
std::shared_ptr<M> generate_matrix(const Grid& grid);

std::shared_ptr<M> generate_connectivity(const Grid& grid);

template <typename FEM>
std::shared_ptr<V> generate_rhs(const Grid& grid);

std::shared_ptr<V> generate_dirichlet_nodes(const Grid& grid);

std::shared_ptr<V> generate_domain_count(const Grid& grid);

std::shared_ptr<V> generate_exact_solution(const Grid& grid);

std::shared_ptr<M> generate_interpolation_matrix(const Grid& grid);

#endif //DUNE_BLOCKSTRUCTURED_PRECONDITIONER_GENERATION_HH
