#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/pdelab/constraints/conforming.hh>
#include <dune/pdelab/backend/istl.hh>
#include <dune/pdelab/gridoperator/gridoperator.hh>
#include <dune/pdelab/function/callableadapter.hh>
#include <dune/blockstructured-preconditioner/backend/numpy.hh>
#include <dune/blockstructured-preconditioner/localoperator/poisson.hh>
#include <dune/blockstructured-preconditioner/localoperator/local_to_global.hh>
#include <dune/blockstructured-preconditioner/localoperator/domain_count.hh>
#include <dune/blockstructured-preconditioner/localoperator/interpolation.hh>
#include "generation.hh"


template <typename FEM>
std::shared_ptr<M> generate_matrix(const Grid& grid){
  auto gv = grid.gridView();

  FEM fem{};

  using GFS = Dune::PDELab::GridFunctionSpace<TrueGV, FEM, Dune::PDELab::NoConstraints, VB>;
  GFS gfs(gv, fem);

  Params p{};

  using LOP = PoissonLocalOperator<Params, FEM::blocks>;
  LOP lop(p);

  using GOP = Dune::PDELab::GridOperator<GFS, GFS, LOP, MB, RT, RT, RT>;
  GOP go(gfs, gfs, lop, MB{});

  using X = typename GOP::Domain;
  X x(gfs, 0.0);

  using J = typename GOP::Jacobian;
  J j(go);
  go.jacobian(x, j);

  return j.storage();
}

template std::shared_ptr<M> generate_matrix<FEM_DG>(const Grid& grid);
template std::shared_ptr<M> generate_matrix<FEM_CG>(const Grid& grid);
template std::shared_ptr<M> generate_matrix<FEM_COARSE>(const Grid& grid);

std::shared_ptr<M> generate_connectivity(const Grid& grid){
  auto gv = grid.gridView();

  FEM_DG fem_dg{};
  using GFSU = Dune::PDELab::GridFunctionSpace<TrueGV, FEM_DG, Dune::PDELab::NoConstraints, VB>;
  GFSU gfsu(gv, fem_dg);

  FEM_CG fem_cg{};
  using GFSV = Dune::PDELab::GridFunctionSpace<TrueGV, FEM_CG, Dune::PDELab::NoConstraints, VB>;
  GFSV gfsv(gv, fem_cg);

  using LOP = LocalToGlobalLocalOperator;
  LOP lop{};

  using GOP = Dune::PDELab::GridOperator<GFSU, GFSV, LOP, MB, RT, RT, RT>;
  GOP go(gfsu, gfsv, lop, MB());

  using X = typename GOP::Domain;
  X x(gfsu, 0.0);

  using J = typename GOP::Jacobian;
  J j(go);
  go.jacobian(x, j);

  return j.storage();
}

template <typename FEM>
std::shared_ptr<V> generate_rhs(const Grid& grid){
  auto gv = grid.gridView();

  FEM fem{};

  using GFS = Dune::PDELab::GridFunctionSpace<TrueGV, FEM, Dune::PDELab::NoConstraints, VB>;
  GFS gfs(gv, fem);

  Params params;

  using LOP = PoissonLocalOperator<Params, NBLOCKS>;
  LOP lop{params};

  using GOP = Dune::PDELab::GridOperator<GFS, GFS, LOP, MB, RT, RT, RT>;
  GOP go(gfs, gfs, lop, MB());

  using X = typename GOP::Domain;
  X x(gfs, 0.0);

  auto g = Dune::PDELab::makeGridFunctionFromCallable(gv, [&](const auto& x){return params.g(x);});
  Dune::PDELab::interpolate(g, gfs, x);

  using Y = typename GOP::Range;
  Y r(gfs, 0.0);
  go.residual(x, r);

  return r.storage();
}

template std::shared_ptr<V> generate_rhs<FEM_DG>(const Grid& grid);
template std::shared_ptr<V> generate_rhs<FEM_CG>(const Grid& grid);

std::shared_ptr<V> generate_dirichlet_nodes(const Grid& grid){
  auto gv = grid.gridView();

  FEM_CG fem{};

  using CON = Dune::PDELab::ConformingDirichletConstraints;
  using GFS = Dune::PDELab::GridFunctionSpace<TrueGV, FEM_CG, CON, VB>;
  GFS gfs(gv, fem);

  using CC = typename GFS::template ConstraintsContainer<RT>::Type;
  CC cc = {};
  cc.clear();

  Params params;

  auto bctype = Dune::PDELab::makeBoundaryConditionFromCallable(gv, [&](const auto& x) {return params.is_dirichlet(x);});
  Dune::PDELab::constraints(bctype, gfs, cc);

  using X = Dune::PDELab::Backend::Vector<GFS, RT>;
  X x(gfs, 0.0);
  Dune::PDELab::set_constrained_dofs(cc, 1., x);

  return x.storage();
}

std::shared_ptr<V> generate_domain_count(const Grid& grid){
  auto gv = grid.gridView();

  FEM_CG fem{};

  using LOP = DomainCount<FEM_CG::blocks>;
  LOP lop{};

  using GFS = Dune::PDELab::GridFunctionSpace<TrueGV, FEM_CG, Dune::PDELab::NoConstraints, VB>;
  GFS gfs(gv, fem);

  using GOP = Dune::PDELab::GridOperator<GFS, GFS, LOP, MB, RT, RT, RT>;
  GOP go(gfs, gfs, lop, MB());

  using X = typename GOP::Domain;
  X x(gfs, 0.0), dc(gfs, 0.0);

  go.residual(x, dc);

  return dc.storage();
}

std::shared_ptr<V> generate_exact_solution(const Grid& grid){
  auto gv = grid.gridView();

  FEM_CG fem{};

  using GFS = Dune::PDELab::GridFunctionSpace<TrueGV, FEM_CG, Dune::PDELab::NoConstraints, VB>;
  GFS gfs(gv, fem);

  Params params;

  using LOP = PoissonLocalOperator<Params, NBLOCKS>;
  LOP lop{params};

  using GOP = Dune::PDELab::GridOperator<GFS, GFS, LOP, MB, RT, RT, RT>;
  GOP go(gfs, gfs, lop, MB());

  using X = typename GOP::Domain;
  X x(gfs, 0.0);

  auto g = Dune::PDELab::makeGridFunctionFromCallable(gv, [&](const auto& x){return params.exact(x);});
  Dune::PDELab::interpolate(g, gfs, x);

  return x.storage();
}

std::shared_ptr<M> generate_interpolation_matrix(const Grid& grid){
  auto gv = grid.gridView();

  FEM_CG fem_fine{};
  FEM_COARSE fem_coarse{};

  using GFS_FINE = Dune::PDELab::GridFunctionSpace<TrueGV, FEM_CG, Dune::PDELab::NoConstraints, VB>;
  GFS_FINE gfs_fine(gv, fem_fine);

  using GFS_COARSE = Dune::PDELab::GridFunctionSpace<TrueGV, FEM_COARSE, Dune::PDELab::NoConstraints, VB>;
  GFS_COARSE gfs_coarse(gv, fem_coarse);

  using LOP = interpolationLocalOperator<RT, FEM_CG::blocks>;
  LOP lop{};

  using GOP = Dune::PDELab::GridOperator<GFS_COARSE, GFS_FINE, LOP, MB, RT, RT, RT>;
  GOP go(gfs_coarse, gfs_fine, lop, MB{});

  using X = typename GOP::Domain;
  X x(gfs_coarse, 0.0);

  using J = typename GOP::Jacobian;
  J j(go);
  go.jacobian(x, j);

  return j.storage();
}
