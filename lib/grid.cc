#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "grid.hh"

TrueGV get_gridview(){
  static TrueGrid grid{{1., 1.}, {3, 3}};
  return grid.leafGridView();
}

