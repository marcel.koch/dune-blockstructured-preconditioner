#ifndef DUNE_BLOCKSTRUCTURED_PRECONDITIONER_PARAMS_HH
#define DUNE_BLOCKSTRUCTURED_PRECONDITIONER_PARAMS_HH

#include <dune/common/fvector.hh>
#include "grid.hh"

struct Params{
  template<typename X>
  static bool is_dirichlet(const X& x){
    return true;
  }

//  static RT f(const Dune::FieldVector<RT, dim>& x) {
//    const RT freq = 2 * Dune::StandardMathematicalConstants<RT>::pi();
//    return 2 * freq * freq * (std::sin(freq * x[0]) * std::sin(freq * x[1]));
//  }
  static RT f(const Dune::FieldVector<RT, dim>& x) {
    return 2 * (x[0] * (1 - x[0]) + x[1] * (1 - x[1]));
  }

  static RT g(const Dune::FieldVector<RT, dim>& x){
    return 0;
  }

//  static RT exact(const Dune::FieldVector<RT, dim>& x){
//    const RT freq = 2 * Dune::StandardMathematicalConstants<RT>::pi();
//    return std::sin(freq * x[0]) * std::sin(freq * x[1]);
//  }
  static RT exact(const Dune::FieldVector<RT, dim>& x){
    return x[0] * (1 - x[0]) * x[1] * (1 - x[1]);
  }
};

#endif //DUNE_BLOCKSTRUCTURED_PRECONDITIONER_PARAMS_HH
