#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <pybind11/pybind11.h>
#include <dune/pdelab/backend/simple/sparse.hh>
#include <dune/blockstructured-preconditioner/backend/numpy.hh>

#include "generation.hh"
#include "grid.hh"

#ifndef MODULE_NAME
#define MODULE_NAME blockstructured
#endif

namespace py = pybind11;

PYBIND11_MODULE(MODULE_NAME, m){
  // grid related interfaces
  py::class_<Grid>(m, "Grid")
      .def(py::init<std::array<int, dim>>())
      .def("__str__", &Grid::gridInfo);

  py::register_exception<Dune::Exception>(m, "DuneException");

  m.def("generate_element_matrix", [](const Grid& g) -> py::object {
    return Dune::PDELab::to_numpy(*generate_matrix<FEM_DG>(g));
    }, "store block stiffness matrix in a scipy matrix.");

  m.def("generate_matrix", [](const Grid& g) -> py::object {
    return Dune::PDELab::to_numpy(*generate_matrix<FEM_CG>(g));
  }, "store stiffness matrix in a scipy matrix.");

  m.def("generate_matrix_coarse", [](const Grid& g) -> py::object {
    return Dune::PDELab::to_numpy(*generate_matrix<FEM_COARSE >(g));
  }, "store stiffness matrix in a scipy matrix.");

  m.def("generate_connectivity", [](const Grid& g) -> py::object {
    return Dune::PDELab::to_numpy(*generate_connectivity(g));
  }, "store dg to cg connectivity matrix in a scipy matrix.");

  m.def("generate_rhs", [](const Grid& g) -> py::object {
    return Dune::PDELab::to_numpy(*generate_rhs<FEM_DG>(g));
  }, "store element wise rhs in a numpy vector.");

  m.def("generate_rhs_cg", [](const Grid& g) -> py::object {
    return Dune::PDELab::to_numpy(*generate_rhs<FEM_CG>(g));
  }, "store element wise rhs in a numpy vector.");

  m.def("generate_dirichlet_nodes", [](const Grid& g) -> py::object {
    return Dune::PDELab::to_numpy(*generate_dirichlet_nodes(g));
  }, "store dirichlet info in a numpy vector (cg!).");

  m.def("generate_domain_count", [](const Grid& g) -> py::object {
    return Dune::PDELab::to_numpy(*generate_domain_count(g));
  }, "store number of domains per dof in a numpy vector (cg!).");

  m.def("generate_exact_solution", [](const Grid& g) -> py::object {
    return Dune::PDELab::to_numpy(*generate_exact_solution(g));
  }, "store solution in a numpy vector (cg!).");

  m.def("generate_interpolation_matrix", [](const Grid& g) -> py::object {
    return Dune::PDELab::to_numpy(*generate_interpolation_matrix(g));
  }, "store solution in a numpy vector (cg!).");
}