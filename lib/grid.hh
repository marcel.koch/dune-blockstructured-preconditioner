#ifndef DUNE_BLOCKSTRUCTURED_PRECONDITIONER_GRID_HH
#define DUNE_BLOCKSTRUCTURED_PRECONDITIONER_GRID_HH

#include <dune/grid/yaspgrid.hh>
#include <dune/grid/common/gridinfo.hh>
#include <pybind11/stl.h>

#ifndef _NBLOCKS
#define _NBLOCKS 3
#endif

constexpr std::size_t NBLOCKS = _NBLOCKS;
constexpr int dim = 2;

using TrueGrid = Dune::YaspGrid<dim>;
using TrueGV = typename TrueGrid::LeafGridView;
using DF = typename TrueGrid::ctype;
using RT = double;

class Grid{
public:
  Grid ()
  {
    Dune::FieldVector<double,dim> L(1.0);
    std::array<int,dim> N = {};
    for (int d = 0; d < dim; d++) N[d] = 1;
    g.reset(new TrueGrid(L,N));
  }
  explicit Grid (std::array<int, dim> N)
  {
    Dune::FieldVector<double,dim> L(1.0);
    g.reset(new TrueGrid(L,N));
  }

  const TrueGV gridView() const { return g->leafGridView();}

  std::string gridInfo() const { Dune::gridinfo(*g); return {};}

private:
  std::shared_ptr<TrueGrid> g;
};

TrueGV get_gridview();


#endif //DUNE_BLOCKSTRUCTURED_PRECONDITIONER_GRID_HH
