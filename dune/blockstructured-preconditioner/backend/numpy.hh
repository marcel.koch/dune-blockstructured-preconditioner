#ifndef DUNE_PDELAB_BACKEND_NUMPY_HH
#define DUNE_PDELAB_BACKEND_NUMPY_HH

#include <vector>

#include <pybind11/pybind11.h>
#include <pybind11/stl_bind.h>

#include <dune/pdelab/backend/simple.hh>


namespace Dune{
  namespace PDELab {
    template<typename E>
    using numpy_vector = std::vector<E>;
    // using numpy_vector = std::vector<E>;
    using NumPyVectorBackend = Simple::VectorBackend<numpy_vector>;
    using NumPyMatrixBackend = Simple::SparseMatrixBackend<numpy_vector, std::size_t>;

    template<typename E>
    pybind11::object to_numpy(const numpy_vector<E>& v){
      auto np = pybind11::module::import("numpy");
      auto array = np.attr("array");
      return array(v);
    }

    template<typename E>
    pybind11::object to_numpy(const Simple::SparseMatrixData<numpy_vector, E, std::size_t>& c){
      auto sparse = pybind11::module::import("scipy.sparse");
      auto csr = sparse.attr("csr_matrix");
      return csr(std::make_tuple(c._data, c._colindex, c._rowoffset), std::make_tuple(c._rows, c._cols));
    }
  }
} // end namespace Dune::PDELab


#endif // DUNE_PDELAB_BACKEND_NUMPY_HH
