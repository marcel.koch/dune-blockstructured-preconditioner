#ifndef DUNE_BLOCKSTRUCTUREDQKFEM_HH
#define DUNE_BLOCKSTRUCTUREDQKFEM_HH


#include <dune/common/power.hh>
#include <dune/localfunctions/lagrange/qk.hh>
#include <dune/pdelab/finiteelementmap/finiteelementmap.hh>
#include <dune/pdelab/finiteelementmap/qkdg.hh>
#include <dune/pdelab/finiteelementmap/qkfem.hh>

namespace impl{
  template<int k>
  struct blocks_injector{
    constexpr static int blocks = k;
  };
}


namespace Dune {
  namespace PDELab {


    //! wrap up element from local functions
    //! \ingroup FiniteElementMap
    template<typename D, typename R, std::size_t k, int dim>
    class QkBlockstructuredLocalFiniteElementMap
        : public SimpleLocalFiniteElementMap<Dune::QkLocalFiniteElement<D, R, dim, k>, dim>,
          public ::impl::blocks_injector<k> {

    public:
      //    constexpr static int blocks = k;

      QkBlockstructuredLocalFiniteElementMap() = default;

      static constexpr bool fixedSize() {
        return true;
      }

      static constexpr bool hasDOFs(int codim) {
        if (k == 1)
          return codim == dim;
        else
          return true;
      }

      static constexpr std::size_t size(GeometryType gt) {
        std::size_t acc = 1;
        for (std::size_t i = 0; i < gt.dim(); ++i)
          acc *= k - 1;
        return acc;
      }

      static constexpr std::size_t maxLocalSize() {
        return Dune::StaticPower<k + 1, dim>::power;
      }

    };

    template<typename D, typename R, std::size_t k, int dim>
    class QkBlockstructuredDGLocalFiniteElementMap :
        public Dune::PDELab::QkDGLocalFiniteElementMap<D, R, k, dim>,
        public ::impl::blocks_injector<k> {
    };
  }
}
#endif //DUNE_PERFTOOL_BLOCKSTRUCTUREDQKFEM_HH
