#ifndef DUNE_BLOCKSTRUCTURED_PRECONDITIONER_DOMAIN_COUNT_HH
#define DUNE_BLOCKSTRUCTURED_PRECONDITIONER_DOMAIN_COUNT_HH

#include <dune/pdelab/localoperator/flags.hh>

template<int k>
struct DomainCount:
    public Dune::PDELab::LocalOperatorDefaultFlags{

  static constexpr bool doAlphaVolume = true;

  template<typename R, typename X, typename EG, typename LFSU, typename LFSV>
  void alpha_volume(const EG &eg, const LFSU &lfsu, const X &x, const LFSV &lfsv, R &r) const {
    for (int iy = 0; iy < k + 1; ++iy)
      for (int ix = 0; ix < k + 1; ++ix)
        r.accumulate(lfsv, ix + iy * (k + 1), 1.);
  }
};


#endif //DUNE_BLOCKSTRUCTURED_PRECONDITIONER_DOMAIN_COUNT_HH
