#ifndef DUNE_BLOCKSTRUCTURED_PRECONDITIONER_LOCAL_TO_GLOBAL_HH
#define DUNE_BLOCKSTRUCTURED_PRECONDITIONER_LOCAL_TO_GLOBAL_HH

#include <dune/pdelab/localoperator/flags.hh>

struct LocalToGlobalLocalOperator
    : public Dune::PDELab::LocalOperatorDefaultFlags
{
  static constexpr bool doPatternVolume = true;
  static constexpr bool doAlphaVolume = true;

  LocalToGlobalLocalOperator() = default;

  // define sparsity pattern of operator representation
  template<typename LFSU, typename LFSV, typename LocalPattern>
  void pattern_volume (const LFSU& lfsu, const LFSV& lfsv,
                       LocalPattern& pattern) const
  {
    assert(lfsv.size() == lfsu.size());
    pattern.reserve(lfsv.size());
    for (std::size_t i=0; i<lfsv.size(); ++i)
      pattern.addLink(lfsv,i,lfsu,i);
  }


  template<typename EG, typename LFSU, typename J, typename X, typename LFSV>
  void jacobian_volume(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, J& jac) const
  {
    assert(lfsv.size() == lfsu.size());
    for (std::size_t i=0; i<lfsv.size(); i++)
      jac.accumulate(lfsv,i,lfsu,i,1);
  }
};

#endif //DUNE_BLOCKSTRUCTURED_PRECONDITIONER_LOCAL_TO_GLOBAL_HH
