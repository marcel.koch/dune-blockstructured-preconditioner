#ifndef DUNE_BLOCKSTRUCTURED_PRECONDITIONER_INTERPOLATION_HH
#define DUNE_BLOCKSTRUCTURED_PRECONDITIONER_INTERPOLATION_HH

#include <dune/pdelab/localoperator/flags.hh>
#include <dune/pdelab/localoperator/pattern.hh>
#include <cstddef>


template<typename RF, int k>
struct interpolationLocalOperator
    : public Dune::PDELab::LocalOperatorDefaultFlags,
      public Dune::PDELab::FullVolumePattern{
  static constexpr bool doPatternVolume = true;
  static constexpr bool doAlphaVolume = true;

  interpolationLocalOperator() = default;

  template<typename EG, typename LFSU, typename J, typename X, typename LFSV>
  void jacobian_volume(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, J& jac) const
  {
    static std::vector<typename LFSU::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeType> phi(4);
    for (int iy = 0; iy < k + 1; ++iy) {
      for (int ix = 0; ix < k + 1; ++ix) {
        const Dune::FieldVector<RF, 2> p({ix / double(k), iy / double(k)});
        const auto& basis = lfsu.finiteElement().localBasis();
        basis.evaluateFunction(p, phi);

        for (std::size_t i = 0; i < phi.size(); ++i) {
          jac.accumulate(lfsv, iy * (k + 1) + ix, lfsu, i, phi[i][0]);
        }
      }
    }
  }

  template<typename R, typename X, typename EG, typename LFSU, typename LFSV>
  void alpha_volume(const EG &eg, const LFSU &lfsu, const X &x, const LFSV &lfsv, R &r) const {
    static std::vector<typename LFSU::Traits::FiniteElementType::Traits::LocalBasisType::Traits::RangeType> phi(4);
    for (int iy = 0; iy < k + 1; ++iy) {
      for (int ix = 0; ix < k + 1; ++ix) {
        const Dune::FieldVector<RF, 2> p({ix / double(k), iy / double(k)});
        const auto& basis = lfsu.finiteElement().localBasis();
        basis.evaluateFunction(p, phi);

        for (std::size_t i = 0; i < phi.size(); ++i) {
          r.accumulate(lfsv, iy * (k + 1) + ix, phi[i][0] * x(lfsu, i));
        }
      }
    }
  }
};

#endif //DUNE_BLOCKSTRUCTURED_PRECONDITIONER_INTERPOLATION_HH
