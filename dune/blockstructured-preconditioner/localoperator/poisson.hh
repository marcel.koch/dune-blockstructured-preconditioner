// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifndef DUNE_BLOCKSTRUCTURED_PRECONDITIONER_POISSON_LOP_HH
#define DUNE_BLOCKSTRUCTURED_PRECONDITIONER_POISSON_LOP_HH


#include "dune/localfunctions/lagrange/lagrangecube.hh"
#include <dune/pdelab/finiteelement/localbasiscache.hh>
#include "dune/pdelab/gridfunctionspace/gridfunctionspace.hh"
#include "dune/pdelab/localoperator/idefault.hh"
#include "dune/pdelab/localoperator/flags.hh"
#include "dune/pdelab/localoperator/pattern.hh"
#include "dune/pdelab/finiteelement/localbasiscache.hh"
#include "dune/pdelab/common/quadraturerules.hh"
#include "dune/pdelab/localoperator/defaultimp.hh"

template<typename Params, int k>
struct PoissonLocalOperator
    : public Dune::PDELab::LocalOperatorDefaultFlags
{
  static constexpr bool doPatternVolume = true;
  static constexpr bool doAlphaVolume = true;

  explicit PoissonLocalOperator(Params& params_) : params(params_) {};

  // define sparsity pattern of operator representation
  template<typename LFSU, typename LFSV, typename LocalPattern>
  void pattern_volume (const LFSU& lfsu, const LFSV& lfsv,
                       LocalPattern& pattern) const
  {
    pattern.reserve(16*k*k);
      for (int subel_y = 0; subel_y < k; ++subel_y)
        for (int subel_x = 0; subel_x < k; ++subel_x)
          for (int jy = 0; jy < 2; ++jy)
            for (int jx = 0; jx < 2; ++jx)
              for (int iy = 0; iy < 2; ++iy)
                for (int ix = 0; ix < 2; ++ix)
                  pattern.addLink(lfsv, (subel_y + iy) * (k + 1) + subel_x + ix,
                                 lfsu, (subel_y + jy) * (k + 1) + subel_x + jx);
  }


  template<typename R, typename X, typename EG, typename LFSU, typename LFSV>
  void alpha_volume(const EG &eg, const LFSU &lfsu, const X &x, const LFSV &lfsv, R &r) const {
    auto cell_geo = eg.entity().geometry();
    const auto quadrature_rule = Dune::PDELab::quadratureRule(cell_geo, 7);
    const auto detjac = cell_geo.integrationElement({0., 0.});
    const auto jit = cell_geo.jacobianInverseTransposed({0., 0.});

    for (const auto &qp: quadrature_rule) {
      const auto &js = basisCache.evaluateJacobian(qp.position(), localBasis);
      const auto &phi = basisCache.evaluateFunction(qp.position(), localBasis);

      // compute gradients of basis functions in transformed element
      std::array<Dune::FieldVector<double, 2>, 4> grad = {};
      for (int i = 0; i < 4; ++i) {
        jit.usmv(k, js[i][0], grad[i]);
      }

      const double factor = detjac * qp.weight() / double(k * k);

      for (int subel_y = 0; subel_y < k; ++subel_y)
        for (int subel_x = 0; subel_x < k; ++subel_x) {
          const Dune::FieldVector<double, 2> qp_macro = {(qp.position()[0] + subel_x) / k,
                                                         (qp.position()[1] + subel_y) / k};

          const double f = -params.f(cell_geo.global(qp_macro));

          Dune::FieldVector<double, 2> gradu = {};
          for (int idim0 = 0; idim0 <= 1; ++idim0) {
            for (int iy = 0; iy < 2; ++iy)
              for (int ix = 0; ix < 2; ++ix)
                gradu[idim0] += x(lfsv, (subel_y + iy) * (k + 1) + subel_x + ix) * grad[ix + iy * 2][idim0];
          }

          for (int iy = 0; iy < 2; ++iy)
            for (int ix = 0; ix < 2; ++ix)
              r.accumulate(lfsv, (subel_y + iy) * (k + 1) + subel_x + ix,
                           (f * phi[ix + iy * 2][0] +
                            grad[ix + iy * 2][0] * gradu[0] +
                            grad[ix + iy * 2][1] * gradu[1]) * factor);
        }
    }
  }


  template<typename EG, typename LFSU, typename J, typename X, typename LFSV>
  void jacobian_volume(const EG& eg, const LFSU& lfsu, const X& x, const LFSV& lfsv, J& jac) const
  {
    auto cell_geo = eg.entity().geometry();
    const auto quadrature_rule = Dune::quadratureRule(cell_geo, 2);
    const auto detjac = cell_geo.integrationElement({0., 0.});
    const auto jit = cell_geo.jacobianInverseTransposed({0., 0.});

    for (const auto &qp: quadrature_rule) {
      const auto &js = basisCache.evaluateJacobian(qp.position(), localBasis);

      std::array<Dune::FieldVector<double, 2>, 4> grad = {};
      for (int i = 0; i < 4; ++i) {
        jit.usmv(k, js[i][0], grad[i]);
      }
      const double factor = detjac * qp.weight() / double(k * k);
      for (int subel_y = 0; subel_y < k; ++subel_y)
        for (int subel_x = 0; subel_x < k; ++subel_x)
          for (int jy = 0; jy < 2; ++jy)
            for (int jx = 0; jx < 2; ++jx)
              for (int iy = 0; iy < 2; ++iy)
                for (int ix = 0; ix < 2; ++ix)
                  jac.accumulate(lfsv, (subel_y + iy) * (k + 1) + subel_x + ix,
                                 lfsu, (subel_y + jy) * (k + 1) + subel_x + jx,
                                 (grad[ix + iy * 2][0] * grad[jx + jy * 2][0] +
                                  grad[ix + iy * 2][1] * grad[jx + jy * 2][1]) * factor);
    }
  }

private:
  Params& params;

  using LocalBasis = Dune::Impl::LagrangeCubeLocalBasis<double, double, 2, 1>;
  Dune::PDELab::LocalBasisCache<LocalBasis> basisCache = {};
  LocalBasis localBasis = {};
};

#endif //DUNE_BLOCKSTRUCTURED_PRECONDITIONER_POISSON_LOP_HH
